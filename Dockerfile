FROM maven:3.6.3-openjdk-16-slim AS MAVEN_TOOL_CHAIN


COPY pom.xml /tmp/
COPY src /tmp/src/
WORKDIR /tmp/
RUN mvn package

FROM openjdk:15-jdk-alpine3.12

CMD mkdir /usr/share/app
WORKDIR /usr/share/app
COPY --from=MAVEN_TOOL_CHAIN /tmp/target/app.war ./app.war

ENTRYPOINT ["java", "-jar", "./app.war"]
